[![build status](https://gitlab.com/joshuamiller/django-tictactoe/badges/master/build.svg)](https://gitlab.com/joshuamiller/django-tictactoe/commits/master)

[![coverage report](https://gitlab.com/joshuamiller/django-tictactoe/badges/master/coverage.svg)](https://gitlab.com/joshuamiller/django-tictactoe/commits/master)

# Django Tic-tac-toe

The game is linked to sessions and provides a means to have any number
of two player (with each player being a browser session) games going
without allowing users to hijack other players' sessions. When a user
visits the base URL, they can choose their piece and start a game. At
the game screen, they are provided a link to provide to the second
player to join their game. After this has occurred, that game is linked
to the two browser sessions and the signup token is meaningless. The
longer-lived URL is an easy-to-read game id that can be revisited by the
same session(s) with game progress intact.

## Local setup

1. Install the necessary python packages

   `pip install -r django_tictactoe/requirements.txt`

2. Generate database

   `python manage.py migrate`

3. Start development server and visit https://[IP/localhost]:8000

   `python manage.py runserver 0.0.0.0:8000`

## Testing

1. `pip install tox` on PATH

2. `tox`
