# flake8: noqa
import os

from django.core.exceptions import ImproperlyConfigured

from . import *


def get_env_variable(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)

SECRET_KEY = get_env_variable('DJANGO_SECRET_KEY')

STATIC_ROOT = '/var/www/html/static'

DEBUG = False

ALLOWED_HOSTS = ['138.68.23.50', 'tictactoe.joshuamiller.co'] # UPDATE WITH YOUR PRODUCTION IP

# Example of a postgresql config
# DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.postgresql_psycopg2',
#        'NAME': get_env_variable('POSTGRES_DATABASE_NAME'),
#        'USER': get_env_variable('POSTGRES_USER'),
#        'PASSWORD': get_env_variable('POSTGRES_PASSWORD'),
#        'HOST': 'localhost',
#        'PORT': '',
#    }
#}
