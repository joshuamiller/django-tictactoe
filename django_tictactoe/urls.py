from django.conf.urls import url, include

urlpatterns = [
    url(r'',
        include('django_tictactoe.tictactoe.urls', namespace='tictactoe')),
]
