class SessionHackMiddleware:
    """Middleware to work-around SESSION_SAVE_EVERY_REQUEST not working.

    Did not look further into the problem other than even when
    SESSION_SAVE_EVERY_REQUEST is True the session must be saved to create a
    session_key.
    """
    def process_request(self, request):
        request.session.save()
