from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^game/(\d+)/$', views.game, name='game'),
    url(r'^join/([0-9a-zA-Z]{32})/$', views.join, name='join'),
    url(r'^status/(\d+)/$', views.status, name='status'),
    url(r'^move/(\d+)/$', views.move, name='move'),
]
