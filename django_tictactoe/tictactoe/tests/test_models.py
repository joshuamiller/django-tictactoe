from django.test import TestCase
from django.utils.crypto import get_random_string

from ..models import TicTacToeGame
from ..exceptions import TicTacToeInvalidMove


class TicTacToeGameTestCase(TestCase):
    def setUp(self):
        self.x_session_key = get_random_string()
        self.o_session_key = get_random_string()
        self.game = TicTacToeGame.objects.create()

    def test_board_is_empty(self):
        self.assertTrue(self.game.board_is_empty)
        self.game.board = TicTacToeGame.X + self.game.board[1:]
        self.game.save()
        self.assertFalse(self.game.board_is_empty)

    def test_board_is_full(self):
        self.assertFalse(self.game.board_is_full)
        self.game.board = TicTacToeGame.X * 9
        self.game.save()
        self.assertTrue(self.game.board_is_full)

    def test_make_mark(self):
        with self.assertRaises(ValueError):
            self.game.make_mark(-1, TicTacToeGame.X)
        with self.assertRaises(ValueError):
            self.game.make_mark(9, TicTacToeGame.X)
        with self.assertRaises(ValueError):
            self.game.make_mark(3, 'F')
        with self.assertRaises(ValueError):
            self.game.make_mark(3, 'Y')

        self.game.make_mark(0, TicTacToeGame.O)

        with self.assertRaises(TicTacToeInvalidMove):
            self.game.make_mark(0, TicTacToeGame.X)

        self.game.make_mark(8, TicTacToeGame.X)

        with self.assertRaises(TicTacToeInvalidMove):
            self.game.make_mark(4, TicTacToeGame.X)

    def test_whos_turn(self):
        self.assertEqual(self.game.whos_turn, TicTacToeGame.O)
        self.game.board = TicTacToeGame.O + self.game.board[1:]
        self.assertEqual(self.game.whos_turn, TicTacToeGame.X)
        self.game.board = (self.game.board[:1] + TicTacToeGame.X +
                           self.game.board[2:])
        self.assertEqual(self.game.whos_turn, TicTacToeGame.O)

    def test_winner(self):
        self.assertIsNone(self.game.winner())

        no_winner = '{0}{1}_{1}_{0}{0}__'.format(
            TicTacToeGame.O, TicTacToeGame.X)
        self.game.board = no_winner
        self.game.save()
        self.assertIsNone(self.game.winner())

        # horizontal
        x_wins = TicTacToeGame.X * 3 + '_' * 6
        o_wins = '_' * 6 + TicTacToeGame.O * 3
        self.game.board = x_wins
        self.game.save()
        self.assertEqual(TicTacToeGame.X, self.game.winner())
        self.game.board = o_wins
        self.game.save()
        self.assertEqual(TicTacToeGame.O, self.game.winner())

        # vertical
        x_wins = '{0}__{0}__{0}__'.format(TicTacToeGame.X)
        o_wins = '_{0}__{0}__{0}_'.format(TicTacToeGame.O)
        self.game.board = x_wins
        self.game.save()
        self.assertEqual(TicTacToeGame.X, self.game.winner())
        self.game.board = o_wins
        self.game.save()
        self.assertEqual(TicTacToeGame.O, self.game.winner())

        # diagonal
        x_wins = '{0}___{0}___{0}'.format(TicTacToeGame.X)
        o_wins = '__{0}_{0}_{0}__'.format(TicTacToeGame.O)
        self.game.board = x_wins
        self.game.save()
        self.assertEqual(TicTacToeGame.X, self.game.winner())
        self.game.board = o_wins
        self.game.save()
        self.assertEqual(TicTacToeGame.O, self.game.winner())
