var TicTacToe = function() {

	var self = this;

	var game_id = window.location.pathname.split("/")[2];
	var move_url = "/move/" + game_id + "/";
	var status_url = "/status/" + game_id + "/";
	var csrf = Cookies.get("csrftoken");
	var empty_char = '_';

	var board;

	this.turn = function() {
		// Is it our turn?
		return $("#turn").val() !== "false";
	};

	this.mark = function() {
		// Get the player mark
		return $("#player-mark").val();
	};

	this.cell_text = function(cell, text) {
		// Our template style may introduce some extra whitespace and new-line.
		if (text === undefined) {
			return $(cell).children().text().replace(/ |\r\n|\n|\r/g,'');
		}
		else {
			$(cell).children().text(text);
		}
	};

	this.mark_cell = function(cell) {
		// Put our mark in this cell
		self.cell_text(cell, self.mark());
		self.send_move(cell);
	};

	this.update_board = function(board) {
		/* Updates the board. If we are given an empty board, then the game has
		   been reset (or it is the first load on a fresh game, and we should
		   set all cells to blank. */
		var re = new RegExp(empty_char, "g");
		if (board.replace(re, "").length === 0) {
			$(".cell").each(function() {
				self.cell_text($(this), '');
			});
		}
		else {
			$(".cell").each(function() {
				var idx = parseInt($(this).attr("id"));

				if (board.charAt(idx) === empty_char) {
					return;
				}

				var span = $(this).children();

				if (self.cell_text($(this)) !== board.charAt(idx)) {
					self.cell_text($(this), board.charAt(idx));
				}
			});
		}
	};

	this.update_wins = function(data) {
		// Update the wins div
		var your_wins, opponent_wins;
		var you = $("#your-wins"), opponent = $("#opponent-wins");
		if (data.player_num == 0) {
			your_wins = data.player_0_wins;
			opponent_wins = data.player_1_wins;
		}
		else {
			your_wins = data.player_1_wins;
			opponent_wins = data.player_0_wins;
		}
		if (you.text() != your_wins) {
			you.text(your_wins);
		}
		if (opponent.text() != opponent_wins) {
			opponent.text(opponent_wins);
		}
	};

	this.update = function(data){
		/* Updates the DOM to match the current state of the game.

		  data is comprised of:
		    board:"O________"
		    mark:"O"
		    player_0_wins:0
		    player_1_wins:0
		    player_num:0
		    turn:false
		*/
		var mark = $("#player-mark"), turn = $("#turn");

		if (!board || board != data.board) {
			board = data.board;
			self.update_board(board);
		}

		self.update_wins(data);

		if (mark.val() !== data.mark) {
			mark.val(data.mark);
		}

		if ((turn.val() === "true") != data.turn) {
			turn.val(data.turn);
			$("#turn-indicator > span").text(
				data.turn ? "Your Turn" : "Their Turn")
		}
	};

	this.send_move = function(cell) {
		// Send a move (make a mark) to the server
		var data = {"move": $(cell).attr("id"),
					"csrfmiddlewaretoken": csrf};
		$.post(move_url, data);
	};

	this.query_status = function() {
		// Get the status and update the DOM
		var data = {"csrfmiddlewaretoken": csrf};
		$.post(status_url, data, self.update);
	};

};

$(document).on("ready", function() {
	var tictactoe = new TicTacToe();

	// Query for status on an interval
	setInterval(function(){tictactoe.query_status()}, 500);

	// Send a move to the server when an empty box is marked
	$(".cell").click(function() {
		if (tictactoe.cell_text($(this)) !== "") {
			return;
		}

		if (tictactoe.turn()) {
			tictactoe.mark_cell($(this));
		}
	});
});
