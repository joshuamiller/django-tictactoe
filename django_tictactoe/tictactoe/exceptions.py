class TicTacToeException(Exception):
    pass


class TicTacToeInvalidMove(TicTacToeException):
    pass
