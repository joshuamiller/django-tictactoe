import uuid

from django.db import models
from django.core.urlresolvers import reverse

from . import exceptions


def get_token():
    return uuid.uuid4().hex


class TicTacToeGame(models.Model):
    """Tic-Tac-Toe Game model that contains data and logic around game state.

    The game consists of two players (0 and 1) who can be either Xs or Os. The
    game keeps track of player wins and can be reset to start a new game.
    """
    X = 'X'
    O = 'O'
    EMPTY_CHAR = '_'
    VALID_CHARS = [X, O]
    RESET = '_' * 9

    board = models.CharField(
        max_length=9, default=RESET,
        help_text='Game board state. It is a 3x3 grid, so it is comprised of '
        '9 chars, with 3 char to a row.')
    game_token = models.CharField(
        max_length=32, default=get_token, unique=True,
        help_text='Signup token to be used before player_1 session is linked '
        'to game.')
    player_0_session_key = models.CharField(
        max_length=40, help_text='The session key for player 0.')
    player_1_session_key = models.CharField(
        max_length=40,  help_text='The session key for player 1.')
    player_0_wins = models.IntegerField(
        default=0,
        help_text='Number of games player 0 has won.')
    player_1_wins = models.IntegerField(
        default=0,
        help_text='Number of games player 1 has won.')
    o_player = models.SmallIntegerField(
        default=0, help_text='The player that is {}.'.format(O))

    @property
    def board_is_empty(self):
        """Does the board have no player marks?

        Returns:
            bool: True if the board is empty.
        """
        return all(char == self.EMPTY_CHAR for char in self.board)

    @property
    def board_is_full(self):
        """Are there no cells left to fill?

        Returns:
            bool: True if the board is full.
        """
        return all(char != self.EMPTY_CHAR for char in self.board)

    @property
    def whos_turn(self):
        """Who's turn is it?

        Returns:
            str: The character mark (self.X, self.O) who has the next move.
        """
        if self.board_is_empty:
            return self.O
        else:
            if self.board.count(self.X) == self.board.count(self.O):
                return self.O
            else:
                return self.X

    @property
    def join_url(self):
        """Provides the url path for the initial joining of player 1.

        Returns:
            str: The URL path for player 1 to join.
        """
        if not self.game_token:
            return ''

        return reverse('tictactoe:join', args=[self.game_token])

    def is_player(self, session_key):
        """Is this sesssion a player in this game?

        Args:
            session_key (str): The session_key to check for membership.

        Returns:
            bool: The session_key is that of a player.
        """
        return session_key in [self.player_0_session_key,
                               self.player_1_session_key]

    def player_with_mark(self, mark):
        """Get the player number for a mark.

        Args:
            mark (str): The mark the player is using.

        Returns:
            int: The player number.
        """
        if mark == self.O:
            if self.o_player == 0:
                return 0
            else:
                return 1
        else:
            if self.o_player == 0:
                return 1
            else:
                return 0

    def get_player_info(self, session_key):
        """Get the player number and player mark for a session player.

        Args:
            session_key (str): The session_key to get player info for.

        Returns:
            None or tuple: If session_key is not a player of this game, then
                return None. If they are, return (player_num, player_mark).
                For example: (0, 'X').
        """
        if not self.is_player(session_key):
            return None

        if session_key == self.player_0_session_key:
            player_num = 0
        elif session_key == self.player_1_session_key:
            player_num = 1

        if self.o_player == player_num:
            player_mark = self.O
        else:
            player_mark = self.X

        return (player_num, player_mark)

    def status_dict(self, session_key):
        """Get the game status for a session player.

        Args:
            session_key (str): The session_key to get game status for.

        Returns:
            Dict: Consisting of:
                player_num (int): The player number
                mark (str): The player mark
                turn (bool): Is it this player's turn?
                board (str): Board state
                player_0_wins (int): Player 0 win count
                player_1_wins (int): Player 1 win count
        """
        player_num, player_mark = self.get_player_info(session_key)
        if self.o_player == player_num:
            turn = self.whos_turn == self.O
        else:
            turn = not self.whos_turn == self.O

        json_dict = {
            'player_num': player_num, 'mark': player_mark, 'turn':  turn,
            'board': self.board, 'player_0_wins': self.player_0_wins,
            'player_1_wins': self.player_1_wins}
        return json_dict

    def make_mark(self, location, mark):
        """Make a mark at a location on the board.

        Args:
            location (int): The cell to mark. This is a number 0..8 that maps
                to the string representation of the grid.
            mark (str): The mark to make.

        Raises:
            ValueError: `location` is not in 0..8 or `mark` is not in
                VALID_CHARS.
            TicTacToeInvalidMove: The move can not be made since it is not this
                player's turn of the location is not empty.
        """

        location = int(location)

        if not -1 < location < 9:
            raise ValueError('`location` must be in 0..8')
        if mark not in self.VALID_CHARS:
            raise ValueError('`mark` must be in {}'.format(self.VALID_CHARS))
        if self.whos_turn != mark:
            raise exceptions.TicTacToeInvalidMove(
                'it is not {}\'s turn'.format(mark))
        if self.board[location] != self.EMPTY_CHAR:
            raise exceptions.TicTacToeInvalidMove(
                '{} is already full'.format(location))

        self.board = self.board[:location] + mark + self.board[location + 1:]

    def reset_board(self, winner_mark=None):
        """Reset the board and increase win count if a player has won.

        Args:
            winner_mark (str or None): The mark that has won the game.
                If this is None, then no player has won and no win count is
                increased.
        """
        if winner_mark:
            if self.player_with_mark(winner_mark) == 0:
                self.player_0_wins += 1
            else:
                self.player_1_wins += 1

        self.board = self.RESET

        self.save()

    def winner(self):
        """Determine if the game has a winner.

        Returns:
            str or None: The mark of the winner or None if no player has won.
        """

        # Grid representation for ease of use
        grid = [list(self.board[x:x + 3]) for x in range(0, 9, 3)]

        # horizontal
        for row in grid:
            if row.count(self.EMPTY_CHAR) > 0:
                continue
            if all(char == row[0] for char in row):
                return row[0]

        # vertical
        for x in range(3):
            col = [grid[y][x] for y in range(3)]
            if col.count(self.EMPTY_CHAR) > 0:
                continue
            if all(char == grid[0][x] for char in col):
                return grid[0][x]

        # diagonal-down
        char = grid[0][0]
        if char != self.EMPTY_CHAR and char == grid[1][1] == grid[2][2]:
            return char

        # diagonal-up
        char = grid[0][2]
        if char != self.EMPTY_CHAR and char == grid[1][1] == grid[2][0]:
            return char

        return None
