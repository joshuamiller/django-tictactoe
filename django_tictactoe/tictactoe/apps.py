from django.apps import AppConfig


class TicTacToeConfig(AppConfig):
    name = 'django_tictactoe.tictactoe'
