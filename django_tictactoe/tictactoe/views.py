from django.http import JsonResponse, HttpResponseForbidden, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.sites.shortcuts import get_current_site

from .models import TicTacToeGame
from .forms import CreateTicTacToeGameForm
from .exceptions import TicTacToeInvalidMove


def index(request):
    """Simple view to create a game."""
    if request.method != 'POST':
        return render(request, 'tictactoe/index.html',
                      {'form': CreateTicTacToeGameForm()})

    form = CreateTicTacToeGameForm(request.POST)
    if form.is_valid():
        game = TicTacToeGame.objects.create(
            player_0_session_key=request.session.session_key)
        if form.cleaned_data['mark'] == TicTacToeGame.O:
            game.o_player = 0
        else:
            game.o_player = 1
        game.save()
    else:
        return redirect('tictactoe:index')

    return redirect('tictactoe:game', game.id)


def join(request, game_token):
    """Consume token and redirect to short link."""

    try:
        game = TicTacToeGame.objects.get(game_token=game_token)
    except TicTacToeGame.DoesNotExist:
        return redirect('tictactoe:index')

    if (game.player_0_session_key == request.session.session_key or
            game.player_1_session_key):
        return redirect('tictactoe:game', game.id)

    game.player_1_session_key = request.session.session_key
    game.save()

    return redirect('tictactoe:game', game.id)


def game(request, game_id):
    """Provide the raw game board.

    Also provide a link with a token to join the game if you are the first
    player. The token is one-time use and associates the other player's session
    to this game. If you are the second player to join or you rejoin the room
    link, then
    """

    try:
        game = TicTacToeGame.objects.get(id=game_id)
    except TicTacToeGame.DoesNotExist:
        return redirect('tictactoe:index')

    if not game.is_player(request.session.session_key):
        return redirect('tictactoe:index')

    player_num, player_mark = game.get_player_info(request.session.session_key)

    join_url = ''
    if not (game.player_0_session_key and game.player_1_session_key):
        join_url = '{}://{}{}'.format(
            'https' if request.is_secure() else 'http',
            get_current_site(request), game.join_url)

    context = {'join_url': join_url}
    context.update(game.status_dict(request.session.session_key))

    return render(
        request, 'tictactoe/game.html', context)


def status(request, game_id):
    """JSON API endpoint to provide game state to client."""
    game = get_object_or_404(TicTacToeGame, id=game_id)

    if (not game.is_player(request.session.session_key) or
            not request.method == 'POST'):
        return HttpResponseForbidden()

    game = TicTacToeGame.objects.get(id=game_id)
    return JsonResponse(game.status_dict(request.session.session_key))


def move(request, game_id):
    game = get_object_or_404(TicTacToeGame, id=game_id)

    if (not game.is_player(request.session.session_key) or
            not request.method == 'POST' or 'move' not in request.POST):
        return HttpResponseForbidden()

    _, player_mark = game.get_player_info(request.session.session_key)
    try:
        game.make_mark(request.POST['move'], player_mark)
        game.save()
    except TicTacToeInvalidMove:
        return HttpResponse(status=409)

    winner = game.winner()
    if winner or game.board_is_full:
        game.reset_board(winner)

    return JsonResponse(game.status_dict(request.session.session_key))
