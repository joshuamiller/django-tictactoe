from django import forms

from .models import TicTacToeGame


class CreateTicTacToeGameForm(forms.Form):
    mark = forms.ChoiceField(
        label='{} goes first. {} or {}?'.format(
            TicTacToeGame.O, TicTacToeGame.X, TicTacToeGame.O),
        choices=((TicTacToeGame.X, TicTacToeGame.X),
                 (TicTacToeGame.O, TicTacToeGame.O))
    )
